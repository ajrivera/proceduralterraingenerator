﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public GameObject golem;

    public int ampladax = 256;
    public int llargadaz = 256;
    public int amplitud = 20;
    public float noiseScale;

    public int octaves;
    public float lacunarity;
    [Range(0, 1)]
    public float persistence;

    public int seed;
    public Vector2 offset;

    Terrain terrain;

    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startngHeight;
    }
    public SplatHeights[] splatHeights;
    // Start is called before the first frame update
    void Start()
    {
        terrain = this.GetComponent<Terrain>();
        terrain.terrainData = TerrainGen(terrain.terrainData);
        terrain.terrainData.SetAlphamaps(0, 0, TextureMap(terrain.terrainData));
        genGolem(terrain.terrainData);
    }

    private TerrainData TerrainGen(TerrainData td)
    {
        td.heightmapResolution = ampladax + 1;
        td.size = new Vector3(ampladax, amplitud, llargadaz);
        td.SetHeights(0, 0, HeightsGen());
        return td;
    }

    private float[,] HeightsGen()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(ampladax, llargadaz, seed, noiseScale, octaves, lacunarity, persistence, offset);
        return noiseMap;
    }

    // Update is called once per frame
    void Update()
    {
        terrain.terrainData = TerrainGen(terrain.terrainData);
        terrain.terrainData.SetAlphamaps(0, 0, TextureMap(terrain.terrainData));
    }
    private float[,,] TextureMap(TerrainData terrainData)
    {
        terrainData.alphamapResolution = ampladax + 1;
        float[,,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];
        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float terrainHeight = terrainData.GetHeight(y, x);

                float[] splat = new float[splatHeights.Length];

                for (int i = 0; i < splatHeights.Length; i++)
                {
                    if (i == splatHeights.Length - 1 && terrainHeight >= splatHeights[i].startngHeight)
                        splat[i] = 1;
                    else if (terrainHeight >= splatHeights[i].startngHeight && terrainHeight <= splatHeights[i + 1].startngHeight)
                        splat[i] = 1;
                }

                for (int j = 0; j < splatHeights.Length; j++)
                {
                    splatmapData[x, y, j] = splat[j];
                }

            }
        }
        return splatmapData;
    }

    private void genGolem(TerrainData terrainData)
    {
        float freq = 10f;
        int seedNative = 3242;

        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float probNative = Mathf.PerlinNoise((terrainData.alphamapHeight + y + seedNative) / freq, (terrainData.alphamapWidth + x + seedNative) / freq);

                if (probNative > 0.9f)
                {
                    float terrainHeight = terrainData.GetHeight(y, x);
                    GameObject go = Instantiate(golem);
                    go.transform.position = new Vector3(x, terrainHeight, y);
                }
            }
        }
    }
}